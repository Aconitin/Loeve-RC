require("irc")

local ircAPI = function(p)

	local dp = true --> Debug print!
	local p = p or {}
	local api = {}

	--> Setup parameters!
	if dp then print("Setting up parameters!") end
	api.parameters = {}
	api.parameters["server"] = p.server or "irc.oftc.net"
	api.parameters["channel"] = p.channel or "#love"
	api.parameters["nickname"] = p.nickname or "ircWithLoeve"
	api.parameters["threadSleepDuration"] = p.threadSleepDuration or 0.5

	--> Setup variables!
	if dp then print("Setting up variables!") end
	api.qin = {} --> List of messages to be sent!
	api.qout = {} --> List of messages received but not yet processed/fetched!
	api.channels = {}
	api.channels.irca = love.thread.getChannel("irca") --> Thread receives via a!
	api.channels.ircb = love.thread.getChannel("ircb") --> Thread sends via b!
	api.thread = love.thread.newThread([[
	
		require("irc")
		local sleep = require "socket".sleep
	
		local thread = {}
		local dp
		thread.parameters, dp = ...
		local channels = {}
		channels.irca = love.thread.getChannel("irca") --> Thread receives via a!
		channels.ircb = love.thread.getChannel("ircb") --> Thread sends via b!
		
		if dp then print("Instanciating irc!") end
		local sock = irc.new{nick = thread.parameters.nickname}

		if dp then print("Creating hook!") end
		sock:hook("OnChat", function(user, channel, message)
			channels.ircb:push({channel = channel, nickname = user.nick, message = message})
		end)

		if dp then print("Connecting ...") end
		sock:connect(thread.parameters.server)
		sock:join(thread.parameters.channel)
		if dp then print("  OK!") end
		
		while true do
		
			local toSend = channels.irca:pop()
			if toSend then
				sock:sendChat(thread.parameters.channel, toSend)
			end
			
			sock:think()
			sleep(thread.parameters.threadSleepDuration)
		
		end
		
	]])
	
	function api:startup()
		self.thread:start(self.parameters, dp)
	end
	
	function api:update(dt)
	
		local err = self.thread:getError()
		if err then
			print("IRC THREAD ERROR:")
			print(err)
			if self.thread and self.thread.kill then
				self.thread:kill()
			end
		end
		
		while #self.qin > 0 do
		
			self.channels.irca:push(table.remove(self.qin, 1))
		
		end
		
		while true do
			local rec = self.channels.ircb:pop()
			if not rec then
				break
			else
				table.insert(self.qout, rec)
			end
		end
	
	end
	
	function api:send(text)
		table.insert(self.qin, text) --> Messages to send are simple strings!
	end
	
	function api:receive()
		return table.remove(self.qout, 1) --> Messages received are {channel = channel, nickname = user.nick, message = message}!
	end

	return api

end

return ircAPI